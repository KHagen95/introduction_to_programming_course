// custom class of a 3 dimensional vector to demonstrate constructors
public class Vector3D
{
	public float x;
	public float y;
	public float z;
	
	// constructor with parameters - x & y are required
	// z is an optional parameter. Optional parameters always have to go last!
	// we can also use optional parameters in regular methods!
	public Vector3D(float x, float y, float z = 1)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	// we added a parameter constructor so we will have to re-add the parameterless one if we want to use it!
	public Vector3D()
	{
	}
}