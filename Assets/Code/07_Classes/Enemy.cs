using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// defining a class: public (Access Modifier) class (class keyword) Enemy (identifier) : MonoBehaviour (inheriting from MonoBehaviour - useful in Unity context)
// MonoBehaviour allows us to attach our script to game object, use unity specific code like Start() and Update(), transform to access our transform component, ...
public class Enemy : MonoBehaviour
{
	// member variable marked as serialized field - our PC will store this value and Unity can draw it in the inspector because it is "serialized"
	// all values serialized and shown in the inspector can be changed on a per-instance basis!
	[SerializeField] private float _startingHealth = 20;
	[SerializeField] private float _damagePerSecond = 5f;
	[SerializeField] private Enemy _otherEnemy;
		
	private float _currentHealthPoints; // member variable! will serve as "backing field" for our CurrentHealthPoints property!
	public float CurrentHealthPoints
	{
		// getter method - this will just return the field _currentHealthPoints (could return any other float value too)
		get
		{
			// the getter method can also execute other statements if we want to!
			Debug.Log($"Reading current value of health points it is equal to {_currentHealthPoints}");
			return _currentHealthPoints;
		}
		// NOTE: we could add a set { _currentHealthPoints = value } method here as well, if we wanted to. This however would allow every object with a reference
		// to an enemy to write to the healthpoints of this enemy (i.e. change the value from the outside, which may be undesirable).
	}
	//alternative forms of writing a property
	// 1) public float CurrentHealthPoints => _currentHealthPoints;
	// 2) public float CurrentHealthPoints
	// {
		// get { return _currentHealthPoints }
	// }
	// 3) public float CurrentHealthPoints
	// {
		// get => _currentHealthPoints;
	// }
	
	// public float CurrentHealthPoints => _currentHealthPoints;
	// auto-property - c# magic! It does the same as the property above. When we compile our code, it turns this into something similar to a backing field
	// and a property but without us having to write all the code for it. This means, we can now use HealthPoints = 5; or other set operations anywhere in 
	// our enemy class but nowhere else. We will be able to change the value of the internal variable in a private scope.
	// additionally, because the property has been declared public, we will have public get access and any class with a reference to enemy will be able to
	// read from it (i.e. retrieve its current value). NOTE: We can of course also do { private get; set; } which would inverse access rights - readable only
	// in the own class and can be set from anywhere.
	public float HealthPoints { get; private set; }
	
	private void Start()
	{
		// _currentHealthPoints = _startingHealth; // assign our current health to starting health initially
		HealthPoints = _startingHealth; // assign our current health to starting health initially
		int speed = 5; // local variable!
		// Debug.Log($"Name: {gameObject.name} HP: {_currentHealthPoints} - Speed: {speed}");
		Debug.Log($"Name: {gameObject.name} HP: {HealthPoints} - Speed: {speed}");
	}
	
	private void Update()
	{
		// example calculation: 5 = dps, 30 fps (33.3 ms per frame)
		// 1 second = 1000 milliseconds
		// 30 fps, 33.3 ms each: 30 * 33.3 (Time.deltaTime = 33.3ms)
		// 60 fps, 16.6 ms each: 60 * 16.6 (Time.deltaTime = 16.6ms)
		// at 30 fps: 5 * (33.3 / 1000) = 0.0333 ~5 Damage per second!
		// at 60 fps: 5 * (16.6 / 1000) = 0.0166 ~5 Damage pers econd!
		float deltaDamage = _damagePerSecond * Time.deltaTime; // multiplication with Time.deltaTime makes it frame rate independent - see above calculation!
		_otherEnemy.InflictDamage(deltaDamage);
		
		// we can read a returned a bool, but we can not read a returned "void" - because nothing was returned!
		// bool isOtherEnemyAlive = _otherEnemy.IsAlive();
		// float otherEnemyHealht = _otherEnemy.InflictDamage(deltaDamage);
		
		if (!this.IsAlive())
		{
			Destroy(gameObject);
		}
	}
	
	private bool IsAlive()
	{
		// return _currentHealthPoints > 0f;
		return HealthPoints > 0f;
	}
	
	public void InflictDamage(float amount)
	{
		// _currentHealthPoints -= amount * 2;
		HealthPoints -= amount * 2;
		if (!IsAlive())
		{
			return;
		}
		
		// Debug.Log($"Ouch, I received {amount} damage and I still have {_currentHealthPoints} HP!");
		Debug.Log($"Ouch, I received {amount} damage and I still have {HealthPoints} HP!");
	}
}