using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour
{
	[SerializeField] private float _speed = 5f;
	[SerializeField] private int _fps = 10;
	
	private void Awake()
	{
		// this locks the frame rate to never go above the set value in _fps variable (useful to demonstrate the impact of Time.deltaTime)
		Application.targetFrameRate = _fps;
	}
	
    // Update is called once per frame
    void Update()
    {
		// Vector3.right: (1, 0, 0) * _speed = (5, 0, 0)
        transform.Translate(Vector3.right * _speed * Time.deltaTime);
		Debug.Log($"Time: {Time.time} delta time: {Time.deltaTime}");
    }
}
