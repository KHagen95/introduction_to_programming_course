using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelloEnemy : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		// below code does instantiate an object of class Enemy
		// because Enemy is a MonoBehaviour, we are not allowed to do this though!
		// instantiating types is only valid for plain-c# classes
        // Enemy karl = new Enemy();
		// Enemy jeff = new Enemy();
    }
}
