using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstructorExample : MonoBehaviour
{
	private void Start()
	{
		// creating a new vector3d (our custom class) using a constructor with parameters
		Vector3D vectorWithValues = new Vector3D(5f, 2.7f, -10);
		// creating a new vector3d (our custom class) using a parameterless constructor
		Vector3D zeroVector = new Vector3D();
		// checking on the values that are assigned after using the constructor
		Debug.Log($"zeroVector: x: {zeroVector.x}, y: {zeroVector.y}, z: {zeroVector.z},");
		Debug.Log($"vectorWithValues: x: {vectorWithValues.x}, y: {vectorWithValues.y}, z: {vectorWithValues.z},");
		// we can access x down here because it is public - we will not be able to do this if its private! (which we will learn more about next week)
		zeroVector.x = 10f;
		// re-evaluating vectors values
		Debug.Log($"zeroVector: x: {zeroVector.x}, y: {zeroVector.y}, z: {zeroVector.z},");
		
		// accessing our current transform
		Vector3 myPosition = transform.position;
		Debug.Log(myPosition);
		// re-assigning myPosition to a new Vector3 (unitys vector class) by using a constructor!
		myPosition = new Vector3(1f, 2f, 3f);
		Debug.Log(myPosition);
		// re assign myPosition to transform.Position!
		transform.position = myPosition;
	}
}