using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariablesAndDataTypes : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		// Exercise 1
		
		// declare a variable myText
        string myText;
		// initialize variable myText with value "This is my Text"
		myText = "This is my text!";
		// print the value of myText to the console
		Debug.Log(myText);
		// re-assign myText to "Something new"
		myText = "Something new";
		// print the value of myText to the console
		Debug.Log(myText);
		
		// declare a new variable otherText by initializing it to the value of myText
		string otherText = myText;
		Debug.Log(otherText);
		
		// Exercise 2
		
		// we can also define variables of other data types, e.g. float
		float pi = 3.14f; // pay attention to the trailing "f" - its important to let our compiler know we are defining a float!
		// this will print the value 3.14 to the console
		Debug.Log(pi);
		
		// Exercise 3
		
		// declare one variable per type int, float, bool, string
		int x = 5;
		float y = 3.14f;
		bool isTrue = false;
		string name = "Kevin";
		
		// log their values
		Debug.Log(x);
		Debug.Log(y);
		Debug.Log(isTrue);
		Debug.Log(name);
		
		// declare more variables
		int z = 12;
		bool hasMoney = false;
		
		// assign their values to existing variables, log results
		x = z;
		Debug.Log(x);

		hasMoney = isTrue;
		Debug.Log(hasMoney);
		
		// Type Casting
		
		// explicit type casting
		x = (int) pi;
		Debug.Log(x);
		
		short myShort;
		int myInt = 100;
		myShort = (short) myInt;
		Debug.Log(myShort);
		
		// implicit type casting
		myInt = myShort;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
