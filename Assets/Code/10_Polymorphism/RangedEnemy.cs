using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolymorphismLecture
{
	public class RangedEnemy : Enemy
	{
		[SerializeField] private float _xMin;
		[SerializeField] private float _xMax;

		private bool _movingLeft;
		
		public override void Attack()
		{
			Debug.Log("Shoot!");
		}

		public override void Move()
		{
			if (_movingLeft)
			{
				transform.Translate(-_speed * Time.deltaTime, 0, 0);
				_movingLeft = transform.position.x >= _xMin;
			}
			else
			{
				transform.Translate(_speed * Time.deltaTime, 0, 0);
				_movingLeft = transform.position.x >= _xMax;
			}
		}
	}
}