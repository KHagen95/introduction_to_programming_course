using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolymorphismLecture
{
	public class EnemyManager : MonoBehaviour
	{
		// thanks to polymorphism we can store all enemy classes, no matter if RangedEnemy, MeleeEnemy, StationaryEnemy or something
		// entirely different in here, as long as they inherit from enemy!
		[SerializeField] private Enemy[] _allEnemies;
		
		// [SerializeField] private Enemy _regular;
		// [SerializeField] private Enemy _ranged;
		// [SerializeField] private Enemy _melee;
		
		// Start is called before the first frame update
		void Start()
		{
			// leveraging the power of polymorphism, we can now loop through all enemies, call the method Move/Attack()
			// we don't care about implementation details, we just do it!
			for (int i = 0; i < _allEnemies.Length; i++)
			{
				_allEnemies[i].Attack();
			}
			
			// _regular.Attack();
			// _ranged.Attack();
			// _melee.Attack();
		}
		
		void Update()
		{
			// leveraging the power of polymorphism, we can now loop through all enemies, call the method Move/Attack()
			// we don't care about implementation details, we just do it!
			foreach (Enemy enemy in _allEnemies)
			{
				enemy.Move();
			}
			
			// _regular.Move();
			// _ranged.Move();
			// _melee.Move();		
		}
	}
}