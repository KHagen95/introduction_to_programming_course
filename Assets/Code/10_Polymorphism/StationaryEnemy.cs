using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolymorphismLecture
{
	public class StationaryEnemy : Enemy
	{	
		private Transform _playerTransform;
	
		private void Awake()
		{
			// search for the player game object using its tag
			_playerTransform = GameObject.FindWithTag("Player").transform;
		}

		public override void Move()
		{
			// look at will align the z-axis of our transform to point at the specified transform
			transform.LookAt(_playerTransform);
		}
	}	
}