using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolymorphismLecture
{
	public class Enemy : MonoBehaviour
	{
		[SerializeField] private float _hitpoints;
		[SerializeField] protected float _speed;
		
		public virtual void Attack()
		{
			Debug.Log("");
		}		

		public virtual void Move()
		{
			transform.Translate(0, 0, _speed * Time.deltaTime);
		}
	}
}