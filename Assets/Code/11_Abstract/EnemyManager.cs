using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AbstractionLecture
{
	public class EnemyManager : MonoBehaviour
	{
		// thanks to polymorphism we can store all enemy classes, no matter if RangedEnemy, MeleeEnemy, StationaryEnemy or something
		// entirely different in here, as long as they inherit from enemy!
		[SerializeField] private AbstractEnemy[] _allEnemies;
		
		// Start is called before the first frame update
		void Start()
		{
			// leveraging the power of polymorphism, we can now loop through all enemies, call the method Move/Attack()
			// we don't care about implementation details, we just do it!
			for (int i = 0; i < _allEnemies.Length; i++)
			{
				_allEnemies[i].Attack();
			}
		}
		
		void Update()
		{
			// leveraging the power of polymorphism, we can now loop through all enemies, call the method Move/Attack()
			// we don't care about implementation details, we just do it!
			foreach (AbstractEnemy enemy in _allEnemies)
			{
				enemy.Move();
			}		
		}
	}
}