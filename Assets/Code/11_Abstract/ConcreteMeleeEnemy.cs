using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AbstractionLecture
{
	public class ConcreteMeleeEnemy : AbstractEnemy
	{
		[SerializeField] private float _amplitude;
		[SerializeField] private float _frequency;
	
		private Color _mySkinColor;
	
		public override Color SkinColor
		{
			get => Color.green;
			set => _mySkinColor = value;
		}
	
		private Transform _playerTransform;
				
		private void Awake()
		{
			_playerTransform = GameObject.FindWithTag("Player").transform;
		}
		
		public override void Attack()
		{
			Debug.Log("Swing!");
		}
		
		public override void Move()
		{
			Vector3 dir = _playerTransform.position - transform.position;
			dir = dir.normalized * _speed;
		
			float sinOffset = _amplitude * Mathf.Sin(Time.time * _frequency);
		
			transform.Translate(new Vector3(dir.x + sinOffset, dir.y, dir.z) * Time.deltaTime);
		}
	}
}