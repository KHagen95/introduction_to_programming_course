using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AbstractionLecture
{
	public abstract class AbstractEnemy : MonoBehaviour
	{
		[SerializeField] private float _hitpoints;
		[SerializeField] protected float _speed;
		
		public abstract Color SkinColor { get; set; }
		
		public abstract void Attack();
		public abstract void Move();
	}
}