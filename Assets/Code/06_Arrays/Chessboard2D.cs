using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chessboard2D : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        SpriteRenderer whiteTile = GameObject.FindWithTag("WhiteTile").GetComponent<SpriteRenderer>();
		SpriteRenderer blackTile = GameObject.FindWithTag("BlackTile").GetComponent<SpriteRenderer>();
		
		SpriteRenderer[,] gameboard = new SpriteRenderer[8,8];
		
		for (int y = 0; y < 8; y++)
		{
			for (int x = 0; x < 8; x++)
			{
				Vector3 position = new Vector3(x - 4.5f, y - 4.5f, 0);
				if (y % 2 == 0)
				{
					gameboard[x, y] = Instantiate(x % 2 == 0 ? blackTile : whiteTile, position, Quaternion.identity);
				}
				else
				{
					gameboard[x, y] = Instantiate(x % 2 == 0 ? whiteTile : blackTile, position, Quaternion.identity);
				}
			}
		}
		
		// make every third piece green:
		// for (int y = 0; y < 8; y++)
		// {
			// for (int x = 0; x < 8; x++)
			// {
				// // if is third piece: make color green!
				// // if y and x are both having the same remainder, it is "the third" tile
				// if (x % 3 == y % 3)
				// {
					// gameboard[x, y].color = Color.green;
				// }
			// }
		// }
		
		// changing the center piece(s) to yellow:
		// gameboard[4, 4].color = Color.yellow;
		// gameboard[3, 4].color = Color.yellow;
		// gameboard[4, 3].color = Color.yellow;
		// gameboard[3, 3].color = Color.yellow;
		
		// inversing the color pattern:
		for (int y = 0; y < 8; y++)
		{
			for (int x = 0; x < 8; x++)
			{				
				// please note: we already have the tiles, so we don't need to create (Instantiate) them.
				// we can just change their colors!
				if (y % 2 == 0)
				{ 
					gameboard[x, y].color = x % 2 == 0 ? Color.white : Color.black; // spawning these white <-> black were swapped
				}
				else
				{
					gameboard[x, y].color = x % 2 == 0 ? Color.black : Color.white; // spawning these black <-> white were swapped
				}
			}
		}
    }
}
