using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrayExamples : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		// array basics: assigning and accessing values
		int[] myNumbers = {5, -7, 23, 59};
		Debug.Log(myNumbers[2]);
		
		// provoking IndexOutOfRangeException:
		// Debug.Log(myNumbers[4]);
		
		// exercise - getting used to arrys
		int sum = 0;
		sum += myNumbers[0];
		sum += myNumbers[1];
		sum += myNumbers[2];
		sum += myNumbers[3];
		Debug.Log($"Sum of array is {sum}");
		Debug.Log($"Second - Last Element: {myNumbers[1] - myNumbers[myNumbers.Length - 1]}");
		myNumbers[0] = 42;
		int changeAndDivision = myNumbers[0] * myNumbers[1] - (myNumbers[3] / myNumbers[2]);
		Debug.Log($"First = 42, * second element - (myNumbers[3] / myNumbers[2]): {changeAndDivision}");
		
		// we can not add an element at an index that we did not reserve earlier
		// myNumbers[4] = 100;

		// exercise from above - summing all values of an array, but in a for loop!
		sum = 0;
		for(int i = 0; i < myNumbers.Length; i++)
		{
			sum += myNumbers[i];
		}
		Debug.Log($"Sum is {sum}");
		
		// example of assigning 1000 random values, then increasing their value by 1 each.
		// demonstration of when loops and arrays work together extremely well!
		// if we had to do this by hand, we would have to manually adjust 1000 values.. means at least 1000 lines of code!
		int[] randomNumbers = new int[1000];
		for(int i = 0; i < randomNumbers.Length; i++)
		{
			randomNumbers[i] = Random.Range(0, 1000);
			// Debug.Log($"Random Number with index {i} is equal to {randomNumbers[i]}");
		}
		
		for(int i = 0; i < randomNumbers.Length; i++)
		{
			randomNumbers[i] = randomNumbers[i] + 1;
			// Debug.Log($"Random Number with index {i} is equal to {randomNumbers[i]}");
		}
		
		// exercise 10000 random values
		int[] randomValues09 = new int[10000];	// contains our random values that can be between [0, 9]
		int[] resultsBuffer = new int[10];		// will contain how often each digit has been rolled. result for 0s will be at index 0 etc
		for(int i = 0; i < randomValues09.Length; i++)
		{
			randomValues09[i] = Random.Range(0, 10);
			// exemplary iteration, first loop iteration, no values assigned yet.
			// i = 0, randomValues09[i] = 5
			// resultsBuffer[0]: 0,
			// resultsBuffer[1]: 0,
			// resultsBuffer[2]: 0,
			// resultsBuffer[3]: 0,
			// resultsBuffer[4]: 0,
			// resultsBuffer[5]: 0,
			// ...
			//resultsBuffer[5] = resultsBuffer[5] + 1;
			resultsBuffer[randomValues09[i]]++;
			// resultsBuffer[0]: 0,
			// resultsBuffer[1]: 0,
			// resultsBuffer[2]: 0,
			// resultsBuffer[3]: 0,
			// resultsBuffer[4]: 0,
			// resultsBuffer[5]: 1,
			// ...
		}
		
		int digitWithMostOccurences = 0;	// store information of which digit has been rolled the most
		int occurences = 0;					// how often did it occur?
		for(int i = 0; i < resultsBuffer.Length; i++)
		{
			Debug.Log($"Digit {i} - {resultsBuffer[i]} occurences");
			if (resultsBuffer[i] > occurences)
			{
				digitWithMostOccurences = i;
				occurences = resultsBuffer[i];
			}
		}
		
		Debug.Log($"Digit {digitWithMostOccurences} occured the most often, with {occurences} occurences");
		
		// example of a foreach loop
		sum = 0;
		foreach(int x in myNumbers)
		{
			sum += x;
		}
		Debug.Log($"Sum is {sum} (says the foreach loop)");
		
		int[] myIntArray;
		
		// some other code 
		
		myIntArray = new int[100];
		for(int i = 0; i < myIntArray.Length; i++)
		{
			myIntArray[i] = Random.Range(0, 100);
		}
		Debug.Log($"myIntArray has number {myIntArray[1]} at second place");
		
		// this will lead to a compile error - we need to make sure that the count we give in [] matches the elements we provide
		// int[] myInitializedAndAssignedArrayWithUnmatchingCount = new int[4]{2, 4, 5, 7, 8, 9, 9,10};
		// for(int i = 0; i < myInitializedAndAssignedArrayWithUnmatchingCount.Length; i++)
		// {
			// myInitializedAndAssignedArrayWithUnmatchingCount[i] = Random.Range(0, 100);
		// }
		
		// we can use arrays that are not as big as they are reserved for initially:
		// int[] myArrayWithJustSomeElements = new int[5];
		// for(int i = 0; i < 2; i++)
		// {
			// myArrayWithJustSomeElements[i] = Random.Range(0, 10);
		// }
		// myArrayWithJustSomeElements[0] = 5;
		// myArrayWithJustSomeElements[1] = 10;
    }
}
