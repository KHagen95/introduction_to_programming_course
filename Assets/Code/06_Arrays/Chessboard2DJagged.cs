using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chessboard2DJagged : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {        
        SpriteRenderer whiteTile = GameObject.FindWithTag("WhiteTile").GetComponent<SpriteRenderer>();
		SpriteRenderer blackTile = GameObject.FindWithTag("BlackTile").GetComponent<SpriteRenderer>();
		
		SpriteRenderer[][] gameboard = new SpriteRenderer[8][];
		for(int i = 0; i < gameboard.Length; i++)
		{
			gameboard[i] = new SpriteRenderer[8];
		}
		
		for (int y = 0; y < 8; y++)
		{
			for (int x = 0; x < 8; x++)
			{
				Vector3 position = new Vector3(x - 4.5f, y - 4.5f, 0);
				if (y % 2 == 0)
				{
					gameboard[x][y] = Instantiate(x % 2 == 0 ? blackTile : whiteTile, position, Quaternion.identity);
				}
				else
				{
					gameboard[x][y] = Instantiate(x % 2 == 0 ? whiteTile : blackTile, position, Quaternion.identity);
				}
				
				gameboard[x][y].name = "PiecesJagged_" + (x + y * gameboard[x].Length);
			}
		}
		
		// reset gameboard to showcase issues with jagged arrays that have inner arrays with different length
		gameboard = new SpriteRenderer[8][];
		for(int i = 0; i < gameboard.Length; i++)
		{
			gameboard[i] = new SpriteRenderer[Random.Range(4, 9)];
			Debug.Log($"gameboard[i] has length: {gameboard[i].Length}");
		}		
		
		// reverse order - first x, then y because inner array can have varying size
		// to loop over the inner array, we must actually check the length of the current inner array
		// remaining code does not change - now we are going through all rows first, then columns.
		for (int x = 0; x < 8; x++)
		{
			for (int y = 0; y < gameboard[x].Length; y++)
			{
				Vector3 position = new Vector3(x - 4.5f, y - 4.5f, 0);
				if (y % 2 == 0)
				{
					gameboard[x][y] = Instantiate(x % 2 == 0 ? blackTile : whiteTile, position, Quaternion.identity);
				}
				else
				{
					gameboard[x][y] = Instantiate(x % 2 == 0 ? whiteTile : blackTile, position, Quaternion.identity);
				}
				
				// numbering slightly changes - we have to do y + x * currentArrayLength to figure out what # of tile we are spawning
				gameboard[x][y].name = "PiecesJagged_" + (y + x * gameboard[y].Length);
			}
		}
    }
}
