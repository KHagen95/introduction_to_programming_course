using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortingNumbers : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        int[] randomNumbers = new int[6]; // initialize and declare array of size 6
		for(int i = 0; i < randomNumbers.Length; i++)
		{
			randomNumbers[i] = Random.Range(1, 100);	// randomly assign a number from 1 - 99 to current value
			Debug.Log($"Rolled a {randomNumbers[i]}");
		}
		
		// nested for-loop: usually using alphabetical order (i, j, k, ...)
		// we will walk through all numbers, compare them with the number before and swap places
		// until we've gone through all of them often enough to have swapped them with all potential neighbours
		for(int i = 0; i < randomNumbers.Length; i++)
		{
			// inner for loop starts at 1 - we don't need to look at 0th element, as it has no previous element
			for(int j = 1; j < randomNumbers.Length; j++)
			{
				// store previous and current element
				int previousNumber = randomNumbers[j - 1];
				int currentNumber = randomNumbers[j];
				
				// print result to help us reason about the process
				Debug.Log($"Previous Number is: {previousNumber} - Current Number is: {currentNumber}");
				
				// if the current number is bigger that the previous one, we have to swap places (largest -> smallest)
				if (currentNumber > previousNumber)
				{
					randomNumbers[j - 1] = currentNumber;
					randomNumbers[j] = previousNumber;
				}
			}
		}
		
		// just print results
		for(int i = 0; i < randomNumbers.Length; i++)
		{
			Debug.Log($"Index: {i} is #: {randomNumbers[i]}");
		}
    }
}
