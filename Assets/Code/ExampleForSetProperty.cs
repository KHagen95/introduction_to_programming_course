using UnityEngine;

public class MyClass : MonoBehaviour
{
	private MyOtherClassWithProperty _myOtherClassWithPropertyReference;
	
	private void Awake()
	{
		// _myOtherClassWithPropertyReference -> _healthPoints
		// value = 5.5f
		// _myOtherClassWithPropertyReference -> _healthPoints = value (= 5.5f)
		_myOtherClassWithPropertyReference.HealthPoints = 5.5f;
		_myOtherClassWithPropertyReference.SetHealthPoints(5.5f);
	}
}

public class MyOtherClassWithProperty : MonoBehaviour
{
	[SerializeField] private float _healthPoints;
	
	public float HealthPoints
	{
		get => _healthPoints;
		// the two set statements below are equivalent to another and do the same thing!
		// set => _healthPoints = value;
		set
		{
			_healthPoints = value;
		}
	}
	
	public void SetHealthPoints(float amount)
	{
		_healthPoints = amount;
	}
}