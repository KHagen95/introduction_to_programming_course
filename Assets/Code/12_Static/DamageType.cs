using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LectureStatic
{
	public enum DamageType
	{
		Slashing,
		Piercing,
		Bludgeoning,
		Poison = 100, // 100
		Fire, // 101
		Water, // 102
		Earth, // 103
	}
}