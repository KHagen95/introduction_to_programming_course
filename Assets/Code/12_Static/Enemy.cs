using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LectureStatic
{
	public class Enemy : MonoBehaviour
	{
		public static int EnemyCount = 0;
		
		[SerializeField] private float _health;
		[SerializeField] private DamageType _immuneToType;
		
		public float Health => _health;
		
		private void Awake()
		{
			EnemyCount++;
		}
		
		private void Update()
		{
			Debug.Log($"I am immune to: {_immuneToType} which is = {(int) _immuneToType}");
		}
		
		private void OnDestroy()
		{
			EnemyCount--;
		}
		
		public void ApplyDamage(float amountOfDamage, DamageType typeOfDamage)
		{
			// checking for immunity
			if (typeOfDamage.Equals(_immuneToType))
			{
				Debug.Log($"Enemy {name} is immune to {typeOfDamage} so it does not receive {amountOfDamage} damage");
				return;
			}
			
			// checking for critical hits
			if (typeOfDamage.Equals(DamageType.Earth))
			{
				amountOfDamage *= 2f;
			}
			
			// apply dmg
			_health -= amountOfDamage;
			if (_health <= 0)
			{
				Destroy(gameObject);
			}
		}
	}
}