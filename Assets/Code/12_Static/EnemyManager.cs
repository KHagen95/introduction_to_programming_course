using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LectureStatic
{
	public class EnemyManager : MonoBehaviour
	{
		[SerializeField] private Enemy _enemy;
		[SerializeField] private DamageType _attackType;
		[SerializeField] private float _damageAmount;
		
		private void Start()
		{
			Debug.Log(_enemy.Health);
		}
		
		private void Update()
		{
			Debug.Log($"Current Enemy Count is equal to {Enemy.EnemyCount}");
			
			if (Input.GetKeyDown(KeyCode.A))
			{
				_enemy.ApplyDamage(_damageAmount, _attackType);
			}
		}
	}
}