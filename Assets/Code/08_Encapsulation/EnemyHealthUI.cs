using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthUI : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Enemy enemy = FindObjectOfType<Enemy>();
		Debug.Log($"Enemy {enemy.name} has {enemy.HealthPoints} HP");
		Debug.Log($"Enemy {enemy.name} has {enemy.CurrentHealthPoints} HP");
		enemy.InflictDamage(enemy.CurrentHealthPoints / 2f);
		Debug.Log($"Enemy {enemy.name} has {enemy.CurrentHealthPoints} HP after inflicting damage");
		// enemy.CurrentHealthPoints = enemy.CurrentHealthPoints * 20;
		// Debug.Log($"Enemy {enemy.name} has {enemy.CurrentHealthPoints} HP after multiplying by 20");
    }
}
