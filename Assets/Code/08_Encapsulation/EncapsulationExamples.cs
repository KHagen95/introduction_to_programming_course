using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncapsulationExamples : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		// {} is defining a new code block!
		{
			int myVariable = 10;
			Debug.Log(myVariable);
		}
        
		// myVariable is out of scope, we can not use it here!
		// Debug.Log(myVariable);
    }
}
