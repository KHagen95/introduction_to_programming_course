using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInheritanceExample : MonoBehaviour
{
	[SerializeField] private int _healthPoints;
	
	protected void Walk()
	{
		Debug.Log("Approaching the player!");
	}
	
	protected void Attack()
	{
		Debug.Log("Attacking the player!");
		
		float roll = Random.value; // [0, 1] - if bigger than 0.5 = 50% 
		if (roll > 0.7f)
		{
			Debug.Log("Kill the player!");
		}
	}
}
