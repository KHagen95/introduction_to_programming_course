using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrcInheritanceExample : EnemyInheritanceExample
{
	private int _currentCounter;
	
    private void Start()
    {
		Speak();
		Walk();
    }
	
	private void Update()
	{
		if (_currentCounter % 2 == 0)
		{
			Attack();
		}
		else
		{
			Block();
		}
		
		_currentCounter++;
	}
	
	private void Block()
	{
		Debug.Log("I blocked a player attack!");
	}
	
	private void Speak()
	{
		Debug.Log($"I am {name}");
	}
}
