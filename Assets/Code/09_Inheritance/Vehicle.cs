using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vehicle : MonoBehaviour
{
	protected int _amountOfTires;
	
	public int AmountOfTires => _amountOfTires;
	
	private void Start()
	{
		_amountOfTires = 0;
		Honk();
	}
	
	public void Honk()
	{
		Debug.Log($"Hooonk {name}, I have {_amountOfTires} tires!");
		Brake();
	}
	
	private void Brake()
	{
		Debug.Log("I'm braking!");
	}
}
