using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonInheritanceExample : EnemyInheritanceExample
{
	[SerializeField] private int _amountOfBones;
	
    // Start is called before the first frame update
    private void Start()
    {
		Debug.Log($"I have {_amountOfBones} bones in my body!");
		Walk();
    }
	
	private void Update()
	{
		Attack();
	}
}
