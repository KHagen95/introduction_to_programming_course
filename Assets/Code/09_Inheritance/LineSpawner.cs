using UnityEngine;

public class LineSpawner : MonoBehaviour, IEntitySpawner
{
	[SerializeField] private int _amountToSpawn;
	[SerializeField] private Entity _entityToSpawn;
	
	public int Amount => _amountToSpawn;
	
	public void Spawn(Entity entity)
	{
		// some logic to spawn entities in a straight line
	}
	
	private void Awake()
	{
		Instantiate(_entityToSpawn);
	}
}

public class SpawningManager : MonoBehaviour
{
	[SerializeField] private LineSpawner _lineSpawner;
	[SerializeField] private RadialSpawner _radialSpawner;
	[SerializeField] private Entity _entityToSpawn;
	
	// make this spawning manager use the line/radial spawner down here
}