using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEntitySpawner
{
	int Amount { get; }
	void Spawn(Entity entity);
}