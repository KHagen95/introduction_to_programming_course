using UnityEngine;

public class RadialSpawner : MonoBehaviour, IEntitySpawner
{
	[SerializeField] private int _amountToSpawn;
	
	public int Amount => _amountToSpawn;
	
	public void Spawn(Entity entity)
	{
		// some logic to spawn entities in a circle
	}
}