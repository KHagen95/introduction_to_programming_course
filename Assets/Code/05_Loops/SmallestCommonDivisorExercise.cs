using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallestCommonDivisorExercise : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		// smallest common divisor of two random numbers
		int randomOne = Random.Range(2, 100);
		int randomTwo = Random.Range(2, 100);
		int scd = 0;
		// divide by zero not allowed; all numbers are divisible by 1. skip these cases
		for (int j = 2; j < Mathf.Max(randomOne, randomTwo); j++)
		{
			// definition of smallest common divisor: 
			// if both numbers are divisible by the number without a remainder, they share the divisor
			// the smallest one possible is the one we look for - we can stop as soon as we found a result
			if (randomOne % j == 0 && randomTwo % j == 0)
			{
				scd = j;
				break;
			}
		}
		
		if (scd == 0)
		{
			// scd still 0 means they share no divisor
			Debug.Log("Can not find smallest common divisor for " + randomOne + " and " + randomTwo);
		}
		else
		{
			// scd found, print it
			Debug.Log("Smallest common divisor for " + randomOne + " and " + randomTwo + " is: " + scd);
		}
    }
}
