using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerExercise : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		GameObject templateCube = GameObject.FindWithTag("TemplateObject"); // searching for the object to clone initially - can be reused during the loop!
		
		// exercise for spawning at least one enemy, up to many ones
		int enemyCount = Random.Range(0, 11);
		Debug.Log($"Enemy count roll is {enemyCount}");
		do
		{
			// Get random positions for x and y according to task definition
			float randomX = Random.Range(-10f, 10f);
			float randomY = Random.Range(-5f, 5f);
			// create a vector at our random position
			Vector3 position = new Vector3(randomX, randomY, 0);
			// spawn our template at position and rotation
			Instantiate(templateCube, position, Quaternion.identity);
			// decrease number of enemies so we will eventually finish the loop
			enemyCount--;
			Debug.Log($"Remaining Enemies To spawn: {enemyCount}");
		}
		while (enemyCount > 0);
		
		Debug.Log($"Spawned all the enemies - Got {enemyCount} left");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
