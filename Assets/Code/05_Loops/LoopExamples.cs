using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopExamples : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        int iterator = 0;
		while (iterator < 5)
		{
			Debug.Log($"Counting up to 4, currently at: {iterator}");
			iterator++;
		}
		
		// object spawner:
		// Vector3 position = new Vector3(0, 0, 0);
		// Quaternion rotation = Quaternion.identity;
		// GameObject spawnTemplate = GameObject.FindWithTag("TemplateObject");
		
		// Instantiate(spawnTemplate, position, rotation);
		
		// exercise - spawning objects in a while loop
		iterator = 0;	// reset iterator variable to 0 for our exercise
		GameObject templateCube = GameObject.FindWithTag("TemplateObject"); // searching for the object to clone initially - can be reused during the loop!
		while (iterator < 5)
		{
			Vector3 position = new Vector3(iterator * 1.5f, 0, 0);	// creating a vector for our position
			Instantiate(templateCube, position, Quaternion.identity); // spawning our clone
			iterator++; // dont forget to increase iterator to avoid infinite loop!
		}
		
		Debug.Log("Done Spawning template cubes!");
		
		// while vs do-while loop comparison
		
		int i = 5;
		while (i < 5)
		{
			Debug.Log($"While: i is smaller than 5! i = {i}");
			i++;
		}
		
		i = 5;
		do
		{
			Debug.Log($"Do-While: i is smaller than 5! i = {i}");
			i++;
		}
		while (i < 5);
		
		// exercise - convert first spawn while loop to for loop
		for (int j = 0; j < 5; j++)
		{
			Vector3 position = new Vector3(j, 0, 0);
			Instantiate(templateCube, position, Quaternion.identity);			
		}
		
		// exemplary for loop with break statement
		for (int j = 0; j < 5; j++)
		{
			// whenever j is equal to 3, we will finish the loop prematurely
			// this is a very trivial example but sometimes we will have more complex checks than this
			// like only breaking if any of our 500 enemies has less than 3 health or similar - in those
			// scenarios we will not know whether the condition checked in if will ever be true but IF it is,
			// we want to prematurely exit the loop!
			if (j == 3)
			{
				Debug.Log("Currently at j == 3, breaking!");
				break;
			}
			
			Debug.Log($"For Loop with a break statement. Trying to count up to 4. Currently at {j}");
		}
    }
}
