using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForLoopChessboard : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		GameObject spawnTemplate = GameObject.FindWithTag("TemplateObject");
		for (int y = 0; y < 8; y++)
		{
			for (int x = 0; x < 8; x++)
			{
				if ((x % 2 == 0 && y % 2 == 0) || (x % 2 == 1  && y % 2 == 1))
				{
					Vector3 position = new Vector3(x - 4.5f, y - 4.5f, 0);
					Instantiate(spawnTemplate, position, Quaternion.identity);
				}
			}
		}
		
		// can not access y/x down here!
    }
}
