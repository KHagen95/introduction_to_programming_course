using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OperatorsExample : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		// exercise 1
        int x = 23;
		int y = 1589;
		// correct result: ~5047.287
		// to achieve this over 5048, we must make sure to use floats where appropriate:
		// in the case below its only for the divisions (y / 5) and (y / x) as they may result in non-integer numbers
		// Forgetting to cast to float in front of these (it can be either of the two or both numbers as float), will 
		// result in a mathematical error, e.g.: 8 / 3 = 2 - as we did not cast to float, we can not display decimal information
		// correct result will be achieved by any of the three: 8f / 3f = 8 / 3f = 8f / 3 = 2.666
		Debug.Log(x * x + (y * 3) - (y / 5f) + 1 * (y / (float) x)); 
		
		// Demonstration of the above issue with a simpler equation: Remove the "f" from 3 to see what happens.
		Debug.Log(x / 3f);
		
		// Casting x to float down here does NOT change the type of x - only the type of the result!
		// x will still be of type int
		float z = (float) x;
		Debug.Log(z);
		Debug.Log(x);
		
		// exercise 2
		x = 7;
		y = 28;
		Debug.Log((x * y + (4 * y)) / ((x * 13 + 42) % 21) % 2);
		
		// exercise 3
		x = 3;
		y = 42;
		float res = y;
		res /= 23;
		res *= 2;
		res %= (x + 5);
		res += x;
		res *= y;
		Debug.Log(res);
		
		Debug.Log(123*4 >= 404);
		Debug.Log(42 * 42 != 3111696/ 1764);
		Debug.Log(56 * 23 <= 9843 / 7.5f);
    }
}
