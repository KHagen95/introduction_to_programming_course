using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SineMovement : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		// set the position of our own gameobject to y = 5
		// this will be overriden by our calculations in Update() - if you want to test it, commment the code below "exercise sine movement" out!
        transform.position = new Vector3(0f, 5f, 0f);
    }

    // Update is called once per frame
    void Update()
    {
		// log the current time of the game to the console 
		// 2 seconds after game start will be 2
		// 100 seconds after game start will be 100 ...
        Debug.Log(Time.time);
		
		// exercise sine movement
		
		// define all variables we need
		float frequency = 3.25f; // b of 1st multiply node
		float amplitude = 1.337f; // b of 2nd multiply node
		int maximum = 5; // max input of Mathf Clamp node
		int minimum = -1; // min input of Mathf Clamp node
		
		// logic calculations
		float sineResult;
		sineResult = Mathf.Sin(frequency * Time.time); // calculation done in Mathf Sin node and first multiply node
		sineResult *= amplitude; // multiply sine result with amplitude - second multiply node
		sineResult = Mathf.Clamp(sineResult, minimum, maximum); // clamp node
    
		// assign the position to y = sineResult:
		transform.position = new Vector3(0f, sineResult, 0f); // Create Vector3 node and Set Position node
	}
}
