using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Math_LectureQuestions : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        float myValue = 0.87f;
		float myFloat = Mathf.Clamp01(myValue);
		Debug.Log(myFloat); // will print 0.87
		myValue += 5;
		Debug.Log(Mathf.Clamp01(myValue)); // will print 1.0
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
