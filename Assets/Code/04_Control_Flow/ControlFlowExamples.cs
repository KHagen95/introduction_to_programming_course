using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlFlowExamples : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        int attackRoll = Random.Range(0, 10);
		// string concetanation <-- dont use this! (its prone to errors and not easy to use)
		Debug.Log("Attack Roll is equal to: " + attackRoll + " - this is using a d10, result: " + attackRoll +"/10");
		// string interpolation <-- always use this! (not so prone to errors, easy to use)
		Debug.Log($"Attack Roll is equal to: {attackRoll} this is using a d10, result: {attackRoll}/10");
		if (3 >= attackRoll)
		{
			Debug.Log("You hit the enemy!");
		}
		// exercise 1 - add a meaningful else branch
		else
		{
			Debug.Log("You miss!");
		}
		
		// if statement order example
		attackRoll = Random.Range(0, 20) + 1; // re-using variable from above, no need to re-define!
		bool doesHit = attackRoll < 17; // hits 80% of the time
		bool blockAttack = Random.Range (0, 10) >= 7; // blocks 30% of the time (whenever result of random is 7 or higher)
		bool isCriticalHit = attackRoll == 20; // if attack roll is equal to 20 (highest possible value) we have a crit!
		if (doesHit && !blockAttack) // we hit and the enemy does not block the attack - deal damage
		{
			Debug.Log("You hit the enemy and it does not block your attack!");
		}
		else if (isCriticalHit) // rolled a crit, dealing critical damage and ignoring block!
		{
			Debug.Log("Critical hit - ignoring block and dealing 2x damage!");
		}
		else if (attackRoll == 0) // we miss and hit ourselves - critical failure!
		{
			Debug.Log("You slipped and hit yourself!");
		}
		else // any other case, we just miss the enemy (maybe not hitting them, maybe they block)
		{
			Debug.Log("You missed the enemy!");
		}
		
		// example from above with "bug" fixed!
		// critical hit ignores blocking, so we can always check for it first! This makes sure, we can deal twice damage
		// and ignore the potential block of the enemy, as we'd expect
		if (isCriticalHit) 
		{
			Debug.Log("Critical hit - ignoring block and dealing 2x damage!");
		}
		else if (doesHit && !blockAttack)
		{
			Debug.Log("You hit the enemy and it does not block your attack!");
		}
		// attack roll had been compared to 0 above, which is not possible...
		// need to check for actual lowest value (since we add +1 to our roll above, it can't ever be equal to 0!)
		else if (attackRoll == 1)
		{
			Debug.Log("You slipped and hit yourself!");
		}
		else
		{
			Debug.Log("You missed the enemy!");
		}
		
		// exercise 2 random number between 0 - 1000, ...
		int randomRoll = Random.Range(0, 1001);
		if (randomRoll < 100 || randomRoll > 950) // smaller than 100 or bigger than 950: loss
		{
			Debug.Log("You loose");
		}
		else if (randomRoll == 500) // exactly 500: Bullseye
		{
			Debug.Log("Bullseye!");
		}
		else if (randomRoll == 777) // exactly 777: Jackpot
		{
			Debug.Log("Jackpot");
		}
		else if ((randomRoll > 800 && randomRoll < 950) || (randomRoll > 100 && randomRoll < 250)) // between 100 - 250 or between 800 - 950: Close call!
		{
			Debug.Log("Close Call!");
		}
		else // all other numbers between 100 and 950, except the above cases: win!
		{
			Debug.Log("You win!");
		}
		
		// exercise ternary operator
		// before:
		/*if (3 >= attackRoll)
		{
			Debug.Log("You hit the enemy!");
		}
		else
		{
			Debug.Log("You miss!");
		}*/
		
		/* Multi-Line comment - everything below is commented out and does not get executed
		*
		*
		*/
		
		// after
		string message = 3 >= attackRoll ? "You hit the enemy!" : "You miss";
		Debug.Log(message);
		
		// we can also do this with variables of a different type, e.g. int:
		// before
		int x;
		if (Random.Range(0, 10) > 5)
		{
			x = 20;
		}
		else
		{
			x = 0;
		}
		
		// after
		x = Random.Range(0, 10) > 5 ? 20 : 0; // assigns 20 or 0 depening on the bool reslt of our condition to x
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
