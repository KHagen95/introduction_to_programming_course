using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateTest : MonoBehaviour
{
	[SerializeField] private float _speed;
	
    void Update()
    {
		// Vector3.right = new Vector3(1, 0, 0); : Left, Up, Down, Front, Back, ...
		transform.Translate(Vector3.right * _speed * Time.deltaTime);
    }
}
