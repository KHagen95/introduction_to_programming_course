using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AwakeTest : MonoBehaviour
{
	[SerializeField] private AwakeTest _otherTestObject;
	
	private Rigidbody _rigidbody;
	public Rigidbody Rigidbody => _rigidbody;
	
	private void Awake()
	{
		Debug.Log("Awake!");
		_rigidbody = GetComponent<Rigidbody>(); // GetComponents, GetComponentInChildren<T>, GetComponentsInChildren, ..InParent
		if (_rigidbody == null)
		{
			_rigidbody = gameObject.AddComponent<Rigidbody>();
		}
	}
	
	private void Start()
	{
		Debug.Log("Start!");
		if (_otherTestObject != null)
		{
			_otherTestObject.Rigidbody.AddForce(Vector3.one);
		}		
	}
}
